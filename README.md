[![pipeline status](https://gitlab.com/SparrowOchon/ads-filter/badges/master/pipeline.svg)](https://gitlab.com/SparrowOchon/ads-filter/-/commits/master)
[![Netlify Status](https://api.netlify.com/api/v1/badges/08fb262a-e612-4d04-b0fd-2c3c9a5cbc6c/deploy-status)](https://app.netlify.com/sites/chromium-adblock/deploys)]

# Ad Filter

The following repo is an auto-updating version of an ad blocking filter list built for [Bromite](https://www.bromite.org/). Offering a more up to date and inclusive ad blocking experience.

**NOTE:** Although made for **Bromite** this is compatible with **Chromium** (and forks) directly aswell.

## Features

- Transparent filter list
- No self-hosting required
- Weekly updates
- Auto duplicate rule merging
- Small file footprint

## Usage for Bromite

- `Settings > Ad Blocking > Filters URL`
- Change it to one of the following url
 	-  `https://chromium-adblock.netlify.app/releases/filters.dat` (Better for Chromium)
	-  `https://sparrowochon.gitlab.io/ads-filter/releases/filters.dat` (Better for Bromite)
- Enjoy your advanced adblocking.

## Manual Building/Hosting

- Clone this Repo
- Fetch the files mentioned under **Special Thanks** in the `README` and place them under the `filters` folder
- Run `src/parse-filters.sh` from the root of this repository.
- Lastly convert them into the appropriate format `src/ruleset_converter --input_format=filter-list --output_format=unindexed-ruleset --input_files=filters/final-list.txt --output_file=public/releases/filters.dat`
- To self host the server must return `last-modified` tag in the `HTTP Response`


## Special Thanks

- [uBlockOrigin Team](https://github.com/uBlockOrigin/uAssets)
- [Hoshsadiq](https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt)
- [AdblockPlus + EasyList](https://easylist-downloads.adblockplus.org/abpvn+easylist.txt)
- [EasyPrivacy](https://easylist.to/easylist/easyprivacy.txt)
- [Curben](https://gitlab.com/curben/urlhaus-filter/raw/master/urlhaus-filter-online.txt)

## License

```
	Show don't Sell License Version 1
https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md
	Copyright (c) 2020, Network Silence
		All rights reserved.
```
