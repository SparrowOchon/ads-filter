#!/bin/bash
#================================================================================================
#
# FILENAME :        parse-filters.sh
#
# DESCRIPTION : Filter out duplicate rules and merge APB rules from different provider lists.
#
# USAGE:
#		./parse-filters.sh
#
#
# AUTHOR :   Network Silence        START DATE :    14 August 2020
#
#
# LICENSE :
#	Show don't Sell License Version 1
#   <https://gitlab.com/SparrowOchon/show-dont-sell/raw/master/LICENSE.md>
#
#	Copyright (c) 2020, Network Silence
#		All rights reserved.
#
#===============================================================================================
function main(){
    local rule_file_parent_dir="filters"
    local temp_file="$rule_file_parent_dir/temp-file.txt"
    local ordered_file="$rule_file_parent_dir/order-file.txt"
    local final_file="$rule_file_parent_dir/final-list.txt"

    file_creation "$temp_file"
    concat_files "$rule_file_parent_dir" "$temp_file"
    file_creation "$final_file"
    order_file "$temp_file" "$ordered_file"
    rm "$temp_file"
    remove_duplicates "$ordered_file" "$final_file"
    rm "$ordered_file"
}
function file_creation(){
    local new_file=$1

    if [ ! -f "$new_file" ]; then
        touch "$new_file"
    fi
}
function concat_files(){
    local file_list=$1
    local combine_file=$2

    for filename in "$file_list"/*.txt; do
        [ -e "$filename" ] || continue
        [ "$filename" == "$combine_file" ] && continue
        cat "$filename" >> "$combine_file"
        rm "$filename"
    done
}
function order_file(){
    local unordered_file=$1
    local ordered_file=$2

    sort "$unordered_file" | uniq > "$ordered_file"
}
function remove_duplicates(){
    local dirty_file=$1
    local uniq_file=$2
    local adblock_rule=""
    local rule_pattern=".*[A-Za-z].*"
    local url_pattern="^.*\..*$"

    declare -a Prev_Line=()
    while IFS= read -r abp_rule || [ -n "$abp_rule" ]
    do
        [[ -z "$abp_rule" || ! "$abp_rule" =~ ${rule_pattern} ]] && continue
        readarray -td' ' Cur_Line < <(printf '%s' "${abp_rule/,domain=/ }");
        if [[ "${Prev_Line[0]}" != "${Cur_Line[0]}" ]];then
            # Last rule duplication ended, save to file
            append_to_file "$adblock_rule" "$uniq_file"
            adblock_rule=""
        fi

        if [[ ${Cur_Line[1]} =~ ${url_pattern} ]];then
            if [[ ${#adblock_rule} -ne 0 ]];then
                # Add domain to previous Rule.
                adblock_rule="$adblock_rule|${Cur_Line[1]}"
            else
                # New Rule
                adblock_rule="${Cur_Line[0]},domain=${Cur_Line[1]}"
            fi
        else
            adblock_rule="${Cur_Line[*]}"
        fi
        Prev_Line=("${Cur_Line[@]}")
    done < "$dirty_file"
    append_to_file "$adblock_rule" "$uniq_file"
    unset Prev_Line
}
function append_to_file(){
    local new_data=$1
    local old_file=$2

    if [ ${#new_data} -ne 0 ];then
        # Last line inherently unique
        echo "$new_data" >> "$old_file"
    fi
}

main "$@"
